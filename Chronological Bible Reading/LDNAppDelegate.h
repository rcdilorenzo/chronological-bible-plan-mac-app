//
//  LDNAppDelegate.h
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 7/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "INAppStoreWindow.h"

@class PassageTableController;

@interface LDNAppDelegate : NSObject <NSApplicationDelegate, NSToolbarDelegate, NSUserNotificationCenterDelegate>

@property (assign) IBOutlet INAppStoreWindow *window;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) PassageTableController *passageController;

- (IBAction)saveAction:(id)sender;

- (IBAction)markRead:(id)sender;

@end
