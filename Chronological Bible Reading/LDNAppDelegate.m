//
//  LDNAppDelegate.m
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 7/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import "LDNAppDelegate.h"
#import "PassageTableController.h"
#import "Passage.h"

@implementation LDNAppDelegate

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize managedObjectContext = _managedObjectContext;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    if (![Passage hasLoadedPassagesInContext:[self managedObjectContext]]) {
        [Passage loadAllInContext:[self managedObjectContext]];
    }
    [self sendNotificationsForComingWeek];
    
    
    self.passageController = [[PassageTableController alloc] initWithNibName:@"PassageTableController" bundle:nil];
    self.passageController.passageContext = [self managedObjectContext];
    [self.window.contentView addSubview:self.passageController.view];
    self.passageController.view.frame = [(NSView *)self.window.contentView bounds];
    self.passageController.view.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
    
    self.window.titleBarHeight = 50.0f;
    self.window.showsBaselineSeparator = NO;
    self.window.trafficLightButtonsLeftMargin = 13.0f;
    
    NSView *titleBarView = self.window.titleBarView;
    NSSize segmentSize = NSMakeSize(200, 25);
    NSRect segmentFrame = NSMakeRect(NSMidX(titleBarView.bounds) - (segmentSize.width / 2.f)+50,
                                     NSMidY(titleBarView.bounds) - (segmentSize.height / 2.f),
                                     segmentSize.width, segmentSize.height);
    NSSegmentedControl *segment = [[NSSegmentedControl alloc] initWithFrame:segmentFrame];
    [segment setSegmentCount:3];
    [segment setLabel:@"Month" forSegment:0];
    [segment setLabel:@"Year" forSegment:1];
    [segment setLabel:@"Done" forSegment:2];
    [segment setSelectedSegment:1];
    segment.target = self.passageController;
    segment.action = @selector(dateRangeDidChange:);
    [segment setSegmentStyle:NSSegmentStyleTexturedRounded];
    segment.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
    [titleBarView addSubview:segment];
}

- (void)sendNotificationsForComingWeek {
    [NSUserNotificationCenter defaultUserNotificationCenter].delegate = self;
    NSError *error; Passage *passage;
    NSDate *now = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSRange week = [cal rangeOfUnit:NSDayCalendarUnit inUnit:NSWeekCalendarUnit forDate:now];
    NSDate *weekFromNow = [now dateByAddingTimeInterval:week.length * 24 * 60 * 60 - 1];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Passage"];
    request.predicate = [NSPredicate predicateWithFormat:@"(read == NO OR read == nil) AND (due < %@) AND (createdNotification == NO OR createdNotification == nil)", weekFromNow];
    NSArray *passages = [[self managedObjectContext] executeFetchRequest:request error:&error];
    
    NSLog(@"Add notifications for %lu passages", (unsigned long)passages.count);
    for (passage in passages) {
        passage.createdNotification = @YES;
        NSUserNotification *notification = [[NSUserNotification alloc] init];
        notification.title = @"Bible Reading Due";
        notification.subtitle = passage.reference;
        notification.userInfo = @{@"reference": [passage.referenceURL absoluteString]};
        notification.deliveryDate = [passage.due dateByAddingTimeInterval:60*60*6.75];
        [[NSUserNotificationCenter defaultUserNotificationCenter] scheduleNotification:notification];
    }
    [[self managedObjectContext] save:&error];
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification {
    return YES;
}

- (void)userNotificationCenter:(NSUserNotificationCenter *)center didActivateNotification:(NSUserNotification *)notification {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:notification.userInfo[@"reference"]]];
}

- (void)markRead:(id)sender {
    [self.passageController confirmMarkSelectedItemsRead];
}

- (IBAction)openVerse:(id)sender {
    [self.passageController openSelectedItems];
}

// Returns the directory the application uses to store the Core Data store file. This code uses a directory named "com.lightdesign.Chronological_Bible_Reading" in the user's Application Support directory.
- (NSURL *)applicationFilesDirectory
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [[fileManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"com.lightdesign.Chronological_Bible_Reading"];
}

// Creates if necessary and returns the managed object model for the application.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
	
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Chronological_Bible_Reading" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSManagedObjectModel *mom = [self managedObjectModel];
    if (!mom) {
        NSLog(@"%@:%@ No model to generate a store from", [self class], NSStringFromSelector(_cmd));
        return nil;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationFilesDirectory = [self applicationFilesDirectory];
    NSError *error = nil;
    
    NSDictionary *properties = [applicationFilesDirectory resourceValuesForKeys:@[NSURLIsDirectoryKey] error:&error];
    
    if (!properties) {
        BOOL ok = NO;
        if ([error code] == NSFileReadNoSuchFileError) {
            ok = [fileManager createDirectoryAtPath:[applicationFilesDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
        }
        if (!ok) {
            [[NSApplication sharedApplication] presentError:error];
            return nil;
        }
    } else {
        if (![properties[NSURLIsDirectoryKey] boolValue]) {
            // Customize and localize this error.
            NSString *failureDescription = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [applicationFilesDirectory path]];
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setValue:failureDescription forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:101 userInfo:dict];
            
            [[NSApplication sharedApplication] presentError:error];
            return nil;
        }
    }
    
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES, NSInferMappingModelAutomaticallyOption: @YES};
    NSURL *url = [applicationFilesDirectory URLByAppendingPathComponent:@"Chronological_Bible_Reading.storedata"];
    NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    if (![coordinator addPersistentStoreWithType:NSXMLStoreType configuration:nil URL:url options:options error:&error]) {
        [[NSApplication sharedApplication] presentError:error];
        return nil;
    }
    _persistentStoreCoordinator = coordinator;
    
    return _persistentStoreCoordinator;
}

// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) 
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@"Failed to initialize the store" forKey:NSLocalizedDescriptionKey];
        [dict setValue:@"There was an error building up the data file." forKey:NSLocalizedFailureReasonErrorKey];
        NSError *error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];

    return _managedObjectContext;
}

// Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window
{
    return [[self managedObjectContext] undoManager];
}

// Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
- (IBAction)saveAction:(id)sender
{
    NSError *error = nil;
    
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }
    
    if (![[self managedObjectContext] save:&error]) {
        [[NSApplication sharedApplication] presentError:error];
    }
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    // Save changes in the application's managed object context before the application terminates.
    
    if (!_managedObjectContext) {
        return NSTerminateNow;
    }
    
    if (![[self managedObjectContext] commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing to terminate", [self class], NSStringFromSelector(_cmd));
        return NSTerminateCancel;
    }
    
    if (![[self managedObjectContext] hasChanges]) {
        return NSTerminateNow;
    }
    
    NSError *error = nil;
    if (![[self managedObjectContext] save:&error]) {

        // Customize this code block to include application-specific recovery steps.              
        BOOL result = [sender presentError:error];
        if (result) {
            return NSTerminateCancel;
        }

        NSString *question = NSLocalizedString(@"Could not save changes while quitting. Quit anyway?", @"Quit without saves error question message");
        NSString *info = NSLocalizedString(@"Quitting now will lose any changes you have made since the last successful save", @"Quit without saves error question info");
        NSString *quitButton = NSLocalizedString(@"Quit anyway", @"Quit anyway button title");
        NSString *cancelButton = NSLocalizedString(@"Cancel", @"Cancel button title");
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:question];
        [alert setInformativeText:info];
        [alert addButtonWithTitle:quitButton];
        [alert addButtonWithTitle:cancelButton];

        NSInteger answer = [alert runModal];
        
        if (answer == NSAlertAlternateReturn) {
            return NSTerminateCancel;
        }
    }

    return NSTerminateNow;
}

@end
