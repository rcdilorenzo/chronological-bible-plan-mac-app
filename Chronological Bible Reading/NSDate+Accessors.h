//
//  NSDate+Accessors.h
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 11/26/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Accessors)

+ (NSDate *)today;
+ (NSDate *)firstSecondOfTheMonth;
+ (NSDate *)lastSecondOfTheMonth;
- (BOOL)isToday;

- (BOOL)isBefore:(NSDate *)date;

@end
