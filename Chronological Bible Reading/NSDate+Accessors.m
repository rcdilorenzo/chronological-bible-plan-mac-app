//
//  NSDate+Accessors.m
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 11/26/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import "NSDate+Accessors.h"

@implementation NSDate (Accessors)

+ (NSDate *)today {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit fromDate:[NSDate date]];
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

+ (NSDate *)firstSecondOfTheMonth {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *beginning = nil;
    if ([cal rangeOfUnit:NSMonthCalendarUnit startDate:&beginning interval:NULL forDate:[NSDate date]])
        return beginning;
    return nil;
}

+ (NSDate *)lastSecondOfTheMonth {
    NSDate *date = [self firstSecondOfTheMonth];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSRange month = [cal rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
    return [date dateByAddingTimeInterval:month.length * 24 * 60 * 60 - 1];
}

- (BOOL)isToday {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit fromDate:self];
    return [[[NSCalendar currentCalendar] dateFromComponents:components] compare:[NSDate today]] == NSOrderedSame;
}

- (BOOL)isBefore:(NSDate *)date {
    return [self compare:date] == NSOrderedAscending;
}

@end
