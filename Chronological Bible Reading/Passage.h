//
//  Passage.h
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 7/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Passage : NSManagedObject

@property (nonatomic, retain) NSString * reference;
@property (nonatomic, retain) NSDate * due;
@property (nonatomic, retain) NSNumber * read;
@property (nonatomic, retain) NSDate * readAt;
@property (nonatomic, retain) NSNumber * createdNotification;
@property (nonatomic, retain) NSString * text;

- (NSURL *)referenceURL;

+ (void)loadAllInContext:(NSManagedObjectContext *)context;
+ (BOOL)hasLoadedPassagesInContext:(NSManagedObjectContext *)context;

- (NSString *)stringForKey:(NSString *)key;

- (BOOL)isDueToday;
- (BOOL)isOverdue;
- (BOOL)hasBeenRead;

@end
