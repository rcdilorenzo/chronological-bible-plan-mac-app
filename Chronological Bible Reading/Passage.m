//
//  Passage.m
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 7/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import "Passage.h"
#import "NSDate+Accessors.h"

NSDateFormatter *displayFormatter;
NSOperationQueue *bibleAPIQueue;

@implementation Passage

@dynamic reference;
@dynamic due;
@dynamic read;
@dynamic readAt;
@dynamic createdNotification;
@dynamic text;

+ (void)loadAllInContext:(NSManagedObjectContext *)context {
    NSData *fileData; NSDictionary *fileJSON; NSError *error;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"chronological_bible_reading" ofType:@"json"];
    fileData = [NSData dataWithContentsOfFile:path];
    if (!fileData) return;
    fileJSON = [NSJSONSerialization JSONObjectWithData:fileData options:NSJSONReadingMutableContainers error:&error];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    
    for (NSString *dateString in fileJSON.allKeys) {
        Passage *passage = [[Passage alloc] initWithEntity:[NSEntityDescription entityForName:@"Passage" inManagedObjectContext:context] insertIntoManagedObjectContext:context];
        passage.due = [dateFormatter dateFromString:dateString];
        passage.reference = fileJSON[dateString];
    }
}

+ (BOOL)hasLoadedPassagesInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Passage"];
    request.fetchLimit = 1;
    NSError *error;
    return [[context executeFetchRequest:request error:&error] count] > 0;
}

- (void)setRead:(NSNumber *)read {
    [self willChangeValueForKey:@"read"];
    [self setPrimitiveValue:read forKey:@"read"];
    [self didChangeValueForKey:@"read"];
    self.readAt = [NSDate date];
}

- (NSURL *)referenceURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://www.esvbible.org/search/?q=%@", [self relativeReferenceURL]]];
}

- (NSString *)relativeReferenceURL {
    NSString *relativeReferenceURL = [self.reference stringByReplacingOccurrencesOfString:@";" withString:@","];
    relativeReferenceURL = [relativeReferenceURL stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    return relativeReferenceURL;
}

- (NSString *)stringForKey:(NSString *)key {
    if (!displayFormatter) {
        displayFormatter = [[NSDateFormatter alloc] init];
        [displayFormatter setDateStyle:NSDateFormatterMediumStyle];
    }
    if ([key isEqualToString:@"dateString"]) {
        return [displayFormatter stringFromDate:self.due];
    } else if ([key isEqualToString:@"readAtString"]) {
        return [displayFormatter stringFromDate:self.readAt];
    } else {
        id value = [self valueForKey:key];
        return value ? [value description] : @"";
    }
}

//- (NSString *)text {
//    if (![self primitiveValueForKey:@"text"]) {
//        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.esvapi.org/v2/rest/passageQuery?key=IP&passage=%@&output-format=plain-text", [self relativeReferenceURL]]]];
//        if (!bibleAPIQueue) {
//            bibleAPIQueue = [[NSOperationQueue alloc] init];
//            bibleAPIQueue.name = @"Bible API Queue";
//        }
//        NSURLConnection *connection = [NSURLConnection sendSynchronousRequest:request returningResponse:<#(NSURLResponse *__autoreleasing *)#> error:<#(NSError *__autoreleasing *)#>];
//    }
//}

- (BOOL)isDueToday {
    return [self.due isToday];
}

- (BOOL)isOverdue {
    return [self.due isBefore:[NSDate today]];
}

- (BOOL)hasBeenRead {
    return [self.read boolValue];
}

@end
