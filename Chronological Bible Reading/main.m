//
//  main.m
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 7/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
