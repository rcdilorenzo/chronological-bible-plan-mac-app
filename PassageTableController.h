//
//  PassageTableController.h
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 7/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PassageTableController : NSViewController <NSTableViewDelegate, NSTableViewDataSource>

@property (nonatomic, strong) NSManagedObjectContext *passageContext;

- (void)confirmMarkSelectedItemsRead;
- (void)openSelectedItems;
- (void)dateRangeDidChange:(NSSegmentedControl *)control;

@end
