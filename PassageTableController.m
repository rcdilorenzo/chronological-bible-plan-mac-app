//
//  PassageTableController.m
//  Chronological Bible Reading
//
//  Created by Christian Di Lorenzo on 7/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import "PassageTableController.h"
#import "Passage.h"
#import "NSAlert+Popover.h"
#import "NSDate+Accessors.h"

@interface PassageTableController ()
@property (nonatomic, weak) IBOutlet NSTableView *tableView;

@property (nonatomic, strong) NSArray *passages;
@property (nonatomic, strong) NSFetchRequest *fetchRequest;
@end

@implementation PassageTableController

- (void)loadView {
    [super loadView];
    [self.tableView setTarget:self];
    [self.tableView setDoubleAction:@selector(openSelectedItems)];
}

- (NSArray *)passages {
    if (!_passages) {
        if (!_fetchRequest) {
            self.fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Passage"];
            self.fetchRequest.sortDescriptors = [self defaultSort];
            self.fetchRequest.predicate = [self defaultPredicate];
        }
        NSError *error;
        _passages = [self.passageContext executeFetchRequest:self.fetchRequest error:&error];
    }
    return _passages;
}

- (NSArray *)defaultSort {
    return @[[NSSortDescriptor sortDescriptorWithKey:@"due" ascending:YES]];
}

- (NSPredicate *)defaultPredicate {
    return [NSPredicate predicateWithFormat:@"(read == NO OR read == nil)"];
}

- (void)dateRangeDidChange:(NSSegmentedControl *)control {
    switch (control.selectedSegment) {
        case 0:
            self.fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[self defaultPredicate], [NSPredicate predicateWithFormat:@"due < %@", [NSDate lastSecondOfTheMonth]]]];
            self.fetchRequest.sortDescriptors = [self defaultSort];
            break;
        case 2:
            self.fetchRequest.predicate = [NSPredicate predicateWithFormat:@"read == YES"];
            self.fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"due" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"readAt" ascending:NO]];
            break;
        default:
            self.fetchRequest.predicate = [self defaultPredicate];
            self.fetchRequest.sortDescriptors = [self defaultSort];
            break;
    }
    self.passages = nil;
    [self.tableView reloadData];
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSTableCellView *cell = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    Passage *passage = self.passages[row];
    cell.textField.stringValue = [passage stringForKey:tableColumn.identifier];
    NSTableRowView *rowView = [tableView rowViewAtRow:row makeIfNecessary:NO];
    
    if ([passage isOverdue]) {
        rowView.backgroundColor = [NSColor redColor];
    } else if ([passage isDueToday]) {
        rowView.backgroundColor = [NSColor greenColor];
    } else {
        [self setAlternatingRowColor:rowView forRow:row];
    }
    if ([passage hasBeenRead]) {
        [self setAlternatingRowColor:rowView forRow:row];
        if ([tableColumn.identifier isEqualToString:@"dateString"]) {
            cell.textField.stringValue = [passage stringForKey:@"readAtString"];
        }
    }
    return cell;
}

- (void)setAlternatingRowColor:(NSTableRowView *)rowView forRow:(NSInteger)row {
    if (row % 2 == 1) {
        rowView.backgroundColor = nil;
    } else {
        rowView.backgroundColor = [NSColor colorWithRed:0.95f green:0.96f blue:0.98f alpha:1.00f];
    }
}

- (BOOL)tableView:(NSTableView *)tableView shouldReorderColumn:(NSInteger)columnIndex toColumn:(NSInteger)newColumnIndex {
    return NO;
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return self.passages.count;
}

- (void)confirmMarkSelectedItemsRead {
    BOOL singleItemSelected = self.tableView.selectedRowIndexes.count == 1;
    NSAlert *alert = [NSAlert alertWithMessageText:[NSString stringWithFormat:@"Are you sure you want to mark %@ read?", singleItemSelected ? @"this item" : [NSString stringWithFormat:@"these %lu items", (unsigned long)self.tableView.selectedRowIndexes.count]]
                                     defaultButton:@"OK"
                                   alternateButton:@"Cancel"
                                       otherButton:nil
                         informativeTextWithFormat:@"Marking %@ as read will put %@ in the done tab and cannot be reversed.", singleItemSelected ? @"this item" : @"these items", singleItemSelected ? @"it" : @"them"];
    alert.icon = nil;
    
    void (^continueBlock)(NSInteger) = ^(NSInteger returnCode){
        if (returnCode == NSModalResponseContinue || returnCode == 1000 || returnCode == 1) {
            [self markSelectedItemsRead];
        } else {
            [self.tableView deselectAll:self];
        }
    };
    
    if (singleItemSelected) {
        [alert runAsPopoverForView:[self.tableView viewAtColumn:0 row:[self.tableView.selectedRowIndexes firstIndex] makeIfNecessary:NO] withCompletionBlock:continueBlock];
    } else {
        continueBlock([alert runModal]);
    }
}

- (void)markSelectedItemsRead {
    for (Passage *passage in [self selectedPassages]) {
        if (![passage hasBeenRead]) {
            passage.read = @YES;
        }
    }
    self.passages = nil;
    [self.tableView reloadData];
    NSError *error;
    [self.passageContext save:&error];
}

- (void)openSelectedItems {
    for (Passage *passage in [self selectedPassages]) {
        [[NSWorkspace sharedWorkspace] openURL:[passage referenceURL]];
    }
}

- (NSArray *)selectedPassages {
    NSMutableArray *passages = [NSMutableArray array];
    if ([[self.tableView selectedRowIndexes] count] > 0) {
        [[self.tableView selectedRowIndexes] enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            [passages addObject:self.passages[idx]];
        }];
    } else {
        [passages addObject:self.passages[[self.tableView rowForView:[self.tableView selectedCell]]]];
    }
    return passages;
}

@end
